#!/bin/bash
set -e

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

pushd "$DIR/Scripts" > /dev/null

python3 build_android.py --prepare "${DIR}"
python3 build_ios.py --prepare "${DIR}"

popd > /dev/null
