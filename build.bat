@echo off

set DIR=%~dp0

pushd "%DIR%\Scripts"

python build_android.py --build --build-demo %DIR% || goto :error
python build_ios.py --build --build-demo %DIR% || goto :error

python export_unity_package.py %DIR% || goto :error

popd
exit /b 0

:error
exit /b %errorlevel%