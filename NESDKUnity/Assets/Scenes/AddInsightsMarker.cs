﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NE.Insights;

public class AddInsightsMarker : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void AddTestMarker()
    {
        bool result = Insights.AddMarker("button_clicked");
        Debug.Log("Added a marker: " + (result ? "Success" : "FAILED"));
    }
}
