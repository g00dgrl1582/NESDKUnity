﻿using UnityEngine;
using UnityEngine.UI;
using NE.Audiences;
using System.Linq;
using System.Collections.Generic;

public class RetrieveAudiences : MonoBehaviour
{
    Text audiencesBox;

    // Start is called before the first frame update
    void Start()
    {
        audiencesBox = gameObject.GetComponent<Text>();
        InvokeRepeating("UpdateAudiences", 0, 5);
    }

    // Update is called once per frame
    void UpdateAudiences()
    {
        if (audiencesBox == null)
        {
            return;
        }

        List<string> names = Audiences.CurrentMemberships()
            .FindAll(membership => membership.liveness == "live")
            .Select(membership => membership.name).ToList();

        if (names.Count > 0)
        {
            audiencesBox.text = string.Join("\n", names);
        }
    }
}
