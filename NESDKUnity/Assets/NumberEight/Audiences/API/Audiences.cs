using System.Collections.Generic;
using System;
using UnityEngine;

#if UNITY_IOS
using System.Runtime.InteropServices; // Includes DllImport
#endif

namespace NE.Audiences
{
    [HelpURL("http://docs.numbereight.ai")]
    public class Audiences : MonoBehaviour
    {
        static Audiences instance;
        private static Audiences Instance
        {
            get
            {
                if (instance == null)
                {
                    GameObject go = new GameObject("Audiences");
                    instance = go.AddComponent<Audiences>();
                }
                return instance;
            }
        }

        [SerializeField]
        public bool isEnabled = true;

        [SerializeField]
        public bool startAutomatically = true;

        [SerializeField]
        public string apiKey;

#if UNITY_IOS
        [DllImport("__Internal")]
        private static extern string NEAudiences_deviceId();

        [DllImport("__Internal")]
        private static extern void NEAudiences_startRecording(string apiKey);

        [DllImport("__Internal")]
        private static extern void NEAudiences_pauseRecording();

        [DllImport("__Internal")]
        private static extern void NEAudiences_stopRecording();

        [DllImport("__Internal")]
        private static extern string NEAudiences_currentMemberships();

        [DllImport("__Internal")]
        private static extern string NEAudiences_currentIds();

        [DllImport("__Internal")]
        private static extern string NEAudiences_currentExtendedIds();

        [DllImport("__Internal")]
        private static extern string NEAudiences_currentIabIds();

        [DllImport("__Internal")]
        private static extern string NEAudiences_currentExtendedIabIds();

        [DllImport("__Internal")]
        private static extern void NEAudiences_deleteUserData();
#elif UNITY_ANDROID
        const string pluginName = "ai.numbereight.audiencesunity.AudiencesPlugin";
        static AndroidJavaClass _pluginClass;
        static AndroidJavaObject _pluginInstance;

        private static AndroidJavaClass PluginClass {
            get {
                if (_pluginClass == null) {
                    _pluginClass = new AndroidJavaClass(pluginName);
                }
                return _pluginClass;
            }
        }

        private static AndroidJavaObject PluginInstance {
            get {
                if (_pluginInstance == null) {
                    _pluginInstance = new AndroidJavaObject(pluginName);
                }
                return _pluginInstance;
            }
        }
#endif

        void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }
            else if (instance != this)
            {
                Destroy(gameObject);
            }
            DontDestroyOnLoad(gameObject);
        }

        void Start()
        {
            if (Instance.startAutomatically)
            {
                StartRecording();
            }
        }

        /**
         * Starts recording device usage to categorise the user into audiences.
         *
         * This will associate an ID with the current device. If NumberEight Insights is in use
         * with a suitable device_id, then that will be used. Otherwise, a new UUID will be created
         * and stored locally.
         *
         * If the SDK is disabled, this will do nothing.
         *
         * Note: due to the mandatory use of a device ID, this data will be treated as
         * Personally Identifiable Information until future releases remove the need for an ID.
         */
        public static void StartRecording()
        {
            if (Instance.isEnabled)
            {
#if UNITY_IOS && !UNITY_EDITOR
                NEAudiences_startRecording(Instance.apiKey);
#elif UNITY_ANDROID && !UNITY_EDITOR
                AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
                AndroidJavaObject activity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
                AndroidJavaObject context = activity.Call<AndroidJavaObject>("getApplicationContext");
                PluginInstance.Call("startRecording", context, Instance.apiKey);
#endif
            }
        }

        /**
         * Pauses any existing Audiences recording whilst preserving the current session.
         * To resume, call `StartRecording` again.
         */
        public static void PauseRecording()
        {
            // You should be able to pause the SDK even if it's `disabled`.
            // Could be disabled while running, should still stop.
            // Will not automatically stop when disabled while running.
            #if UNITY_IOS && !UNITY_EDITOR
            NEAudiences_pauseRecording();
            #elif UNITY_ANDROID && !UNITY_EDITOR
            PluginInstance.Call("pauseRecording");
            #endif
        }

        /**
         * Stops recording device usage for audiences.
         * Will still stop if `isEnabled` is false.
         */
        public static void StopRecording()
        {
            // You should be able to stop the SDK even if it's `disabled`.
            // Could be disabled while running, should still stop.
            // Will not automatically stop when disabled while running.
#if UNITY_IOS && !UNITY_EDITOR
            NEAudiences_stopRecording();
#elif UNITY_ANDROID && !UNITY_EDITOR
            PluginInstance.Call("stopRecording");
#endif
        }

        /**
         * The list of audience memberships detected by NumberEight Audiences.
         * This list changes periodically as the user uses the device more.
         *
         * <returns>
         * A set of audience memberships for the user containing an ID, audience name, and
         * equivalent IAB Audience Taxonomy IDs.
         * e.g.
         *      - Membership("NE-1-1", "Joggers", [ IABAudience("410") ], "live")
         *      - Membership("NE-2-6", "Culture Vultures", [ IABAudience("779") ], "habitual")
         *      - Membership("NE-2-3", "Cinema Buffs", [
         *          IABAudience("467"),
         *          IABAudience("787", [ "PIFI3" ])
         *        ], "habitual")
         *
         * Note: an empty list will be returned if NumberEight Audiences isn't loaded in the current Unity application.
         * </returns>
         */
        public static List<Membership> CurrentMemberships()
        {
            if (!Instance.isEnabled)
            {
                // If SDK not enabled, return empty list.
                return new List<Membership>();
            }

#if UNITY_IOS && !UNITY_EDITOR
            string jsonString = NEAudiences_currentMemberships();
#elif UNITY_ANDROID && !UNITY_EDITOR
            string jsonString = PluginInstance.Call<string>("getCurrentMemberships");
#elif UNITY_EDITOR
            Membership testAudience = new Membership();
            testAudience.id = "NE-0-0";
            testAudience.name = "test-audience";
            testAudience.liveness = "live";

            MembershipArray testArr = new MembershipArray();
            testArr.memberships = new Membership[] { testAudience };
            string jsonString = JsonUtility.ToJson(testArr);
#else
            return new List<Membership>();
#endif

            return new List<Membership>(JsonUtility.FromJson<MembershipArray>(jsonString).memberships);
        }

        private static List<string> GetStringListHelper(Func<string> func)
        {
            if (!Instance.isEnabled)
            {
                return new List<string>();
            }

            string rawString = func();
            return new List<string>(rawString.Split(','));
        }

        /**
         * The list of audience IDs detected by NumberEight Audiences.
         * This is a convenience function to return only the IDs from the full list of memberships.
         *
         * <returns>
         * A set of audience membership IDs for the user.
         * e.g.
         * - "NE-1-1"
         * - "NE-2-6"
         * - "NE-2-3"
         *
         * Note: an empty list will be returned if NumberEight Audiences isn't loaded in the current Unity application.
         * </returns>
         */
        public static List<string> CurrentIds()
        {
            return GetStringListHelper(() => {
#if UNITY_IOS && !UNITY_EDITOR
                return NEAudiences_currentIds();
#elif UNITY_ANDROID && !UNITY_EDITOR
                return PluginInstance.Call<string>("getCurrentIds");
#elif UNITY_EDITOR
                return "";
#endif
            });
        }

        /**
         * The list of audience IDs and their corresponding liveness detected by NumberEight Audiences.
         * This is a convenience function to return only the extended IDs from the list of memberships.
         *
         * This is useful for use cases such as adding audience memberships to ad requests.
         *
         * <returns>
         * A set of extended audience membership IDs for the user.
         * e.g.
         * - "NE-1-1|H"
         * - "NE-100-1|L"
         * - "NE-101-2|T"
         *
         * Note: an empty list will be returned if NumberEight Audiences isn't loaded in the current Unity application.
         * </returns>
         */
        public static List<string> CurrentExtendedIds()
        {
            return GetStringListHelper(() => {
#if UNITY_IOS && !UNITY_EDITOR
                return NEAudiences_currentExtendedIds();
#elif UNITY_ANDROID && !UNITY_EDITOR
                return PluginInstance.Call<string>("getCurrentExtendedIds");
#elif UNITY_EDITOR
                return "";
#endif
            });
        }

        /**
         * The list of IAB Audience Taxonomy IDs detected by NumberEight Audiences.
         * This is a convenience function to return only the IAB IDs from the list of memberships.
         *
         * This is useful for use cases such as adding audience memberships to ad requests.
         *
         * <returns>
         * A set of IAB IDs for the user.
         * e.g.
         * - "408"
         * - "762"
         *
         * Note: an empty list will be returned if NumberEight Audiences isn't loaded in the current Unity application.
         * </returns>
         */
        public static List<string> CurrentIabIds()
        {
            return GetStringListHelper(() => {
#if UNITY_IOS && !UNITY_EDITOR
                return NEAudiences_currentIabIds();
#elif UNITY_ANDROID && !UNITY_EDITOR
                return PluginInstance.Call<string>("getCurrentIabIds");
#elif UNITY_EDITOR
                return "";
#endif
            });
        }

        /**
         * The list of extended IAB Audience Taxonomy IDs detected by NumberEight Audiences.
         * This is a convenience function to return the IAB IDs along with any extensions such
         * as purchase intent from the list of memberships.
         *
         * This format is as described by the IAB Seller-Defined Audiences guidance.
         *
         * This is useful for use cases such as adding audience memberships to ad requests.
         *
         * <returns>
         * A set of IAB IDs for the user.
         * e.g.
         * - "408|PIFI1"
         * - "762|PIFI2|PIPV2"
         *
         * Note: an empty list will be returned if NumberEight Audiences isn't loaded in the current Unity application.
         * </returns>
         */
        public static List<string> CurrentExtendedIabIds()
        {
            return GetStringListHelper(() => {
#if UNITY_IOS && !UNITY_EDITOR
                return NEAudiences_currentExtendedIabIds();
#elif UNITY_ANDROID && !UNITY_EDITOR
                return PluginInstance.Call<string>("getCurrentExtendedIabIds");
#elif UNITY_EDITOR
                return "";
#endif
            });
        }

        /**
         * A device identifier for use in security and error reporting
         */
        public static string DeviceId()
        {
#if UNITY_IOS && !UNITY_EDITOR
            return NEAudiences_deviceId();
#elif UNITY_ANDROID && !UNITY_EDITOR
            AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject activity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
            AndroidJavaObject context = activity.Call<AndroidJavaObject>("getApplicationContext");
            return PluginInstance.Call<string>("getDeviceId", context);
#elif UNITY_EDITOR
            return "The NumberEight Device ID is not available in the Unity Editor.";
#endif
        }

        /**
         * Delete NumberEight's data from the device's local storage.
         *
         * Warning: this will remove all data related to NumberEight, including device identifiers.
         * It will no longer be possible to cross-reference this user to logs or records of consent.
         * Consequently, this will also anonymise any data that NumberEight has stored on its servers.
         */
        public static void DeleteUserData()
        {
#if UNITY_IOS && !UNITY_EDITOR
            NEAudiences_deleteUserData();
#elif UNITY_ANDROID && !UNITY_EDITOR
            AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject activity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
            AndroidJavaObject context = activity.Call<AndroidJavaObject>("getApplicationContext");
            PluginInstance.Call("deleteUserData", context);
#endif
        }
    }
}
