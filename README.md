# NESDKUnity
This is the NumberEight Unity SDK. This allows games and apps made using Unity to have access to NumberEight Audiences and NumberEight Insights, in much the same way they can for native apps using the Android and iOS SDK.

## Installation
You can download the NumberEight Unity SDKs from the offical repo here: https://gitlab.com/numbereight/sdk/NESDKUnity/-/packages

In order to use the SDK, you will need to obtain a NumberEight API key, which you can get through the [portal](https://portal.eu.numbereight.ai/keys).

## Getting Started
Each package contains a `README.md` with all of the information you need to get started.

## Maintainers
* [Matthew Paletta](matt@numbereight.ai)
* [Chris Watts](chris@numbereight.ai)
