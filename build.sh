#!/bin/bash
set -e

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

pushd "$DIR/Scripts" > /dev/null

python3 build_android.py --build --build-demo "${DIR}"
python3 build_ios.py --build --build-demo "${DIR}"

python3 export_unity_package.py "${DIR}"

popd > /dev/null
