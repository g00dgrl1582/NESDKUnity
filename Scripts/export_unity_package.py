import constants
from build_shared import *
import sys
import os

def export(project_dir, method, package_name):
    push_dir(project_dir)

    build_path = safe_rel_path(sdk_name(), package_name + ".unitypackage")
    output_path = safe_rel_path(constants.export_path(package_name))

    print("\nCreating package at: " + output_path)
    run_unity_method(sdk_name(), method=method)
    
    os.replace(build_path, output_path)
    print("\nCreated package successfully")

    pop_dir()

def export_package(project_dir):
    export(project_dir, "NE.Utils.Exporter.ExportPackage", "NESDKUnity")

def export_wrapper_package(project_dir):
    export(project_dir, "NE.Utils.Exporter.ExportWrapperPackage", "NESDKUnityWrapper")

if __name__ == "__main__":
    export_package(project_dir=sys.argv[1])
    export_wrapper_package(project_dir=sys.argv[1])
