#!/usr/bin/env python3
from constants import *
import os
import sys
from distutils.dir_util import copy_tree
import subprocess
import argparse

from build_shared import *


def run_gradle(gradle_shell, step):
    safe_call([gradle_shell, step])

def get_gradle_shell(project_name):
    search_paths = [
        safe_path(project_name),
        safe_path(project_name, "AndroidBuild"),
    ]

    for path in search_paths:
        if os.name == 'nt':
            gradlew = safe_rel_path(path, "gradlew.bat")
        else:
            gradlew = safe_rel_path(path, "gradlew")

        if os.path.exists(gradlew):
            safe_call(["chmod", "+x", gradlew])
            return gradlew

    system_gradle = find_tool("gradle")
    if system_gradle is not None:
        return system_gradle

    raise RuntimeError("Android: Gradle could not be found.")

def build_android_wrapper():
    push_dir(safe_path("Plugins", "Android"))
    gradle_shell = get_gradle_shell(".")

    print("\n" + "Android: Building wrapper plugin")
    run_gradle(gradle_shell, "assembleRelease")

    pop_dir() # Android

def copy_android_sdk():
    copy_plugins_proj(sdk_name())

def copy_android_demo():
    copy_plugins_proj(demo_name())

def copy_plugins_proj(project_name):
    clear_plugins_shared(project_name)
    copy_plugins_shared(project_name)

    print("\n" + project_name + ", Android: Copying built wrapper plugin")
    audiences_proj_plugins = safe_rel_path(project_name, "Assets", "NumberEight", "Audiences", "Plugins", "Android")
    insights_proj_plugins = safe_rel_path(project_name, "Assets", "NumberEight", "Insights", "Plugins", "Android")
    android_audiences_plugin = safe_rel_path("Plugins", "Android", "audiencesunity", "build", "outputs", "aar")
    android_insights_plugin = safe_rel_path("Plugins", "Android", "insightsunity", "build", "outputs", "aar")

    os.makedirs(audiences_proj_plugins, exist_ok=True)
    os.makedirs(insights_proj_plugins, exist_ok=True)

    copy_tree(android_audiences_plugin, audiences_proj_plugins)
    copy_tree(android_insights_plugin, insights_proj_plugins)

def build_android_sdk():
    build_android_proj(sdk_name())

def build_android_demo():
    build_android_proj(demo_name())

def build_android_proj(project_name):
    gradle_shell = get_gradle_shell(project_name)

    print("\n" + project_name + ", Android: Exporting Gradle project")
    run_unity_method(project_name, method="NE.Utils.AutomatedBuild.PerformAndroidBuild")

    print("\n" + project_name + ", Android: Building Unity project")
    build_unity_platform(project_name, platform="Android")

def build_android(project_dir, build_sdk: bool, build_demo: bool):
    push_dir(project_dir)

    build_android_wrapper()
    copy_android_sdk()
    copy_android_demo()

    if build_sdk:
        build_android_sdk()

    if build_demo:
        build_android_demo()

    pop_dir() # project_dir

    return 0

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Prepare and build the Android Unity projects.')
    parser.add_argument('project_dir',
                        help='directory in which the Unity projects are located')
    parser.add_argument('--build', dest='build', action='store_true',
                        help='prepares the plugins and builds NESDKUnity')
    parser.add_argument('--build-demo', dest='build_demo', action='store_true',
                        help='prepares the plugins and builds NESDKUnityDemo')
    parser.add_argument('--prepare', dest='build', action='store_false',
                        help='prepares the plugins without building the Unity projects')

    args = parser.parse_args()

    if os.getenv('ANDROID_SDK_ROOT') is None:
        print("Environment variable ANDROID_SDK_ROOT must be set to the Android SDK location", file=sys.stderr)
        sys.exit(1)

    rc = build_android(project_dir=args.project_dir, build_sdk=args.build, build_demo=args.build_demo)
    sys.exit(rc)
