from distutils.spawn import find_executable
import os

from utils import *

def is_tool(name: str):
    """Check whether `name` is on PATH."""
    return find_tool(name) is not None

def find_tool(name: str):
    return find_executable(name)

unity_version = os.environ.get("UNITY_VERSION", "2020.1.15f1")

use_raw_logs = True

unity_bin = None
if is_tool("u3d"):
    unity_bin = (
        find_executable("u3d") + " run -u " + unity_version + (" -- " if not use_raw_logs else " --raw-logs --")
    )
else:
    possible_paths = [
        "/Applications/Unity/Hub/Editor/{0}/Unity.app/Contents/MacOS/Unity".format(
            unity_version
        ),  # Unity Hub (Mac OS)
        "/Applications/Unity/Unity.app/Contents/MacOS/Unity",  # Unity Hub (Mac App)
        "\"C:\\Program Files\\Unity\\Hub\\Editor\\{0}\\Editor\\Unity.exe\"".format(
            unity_version
        ), # Unity Hub (Windows)
        "~/Unity/Hub/Editor/{0}/Editor/Unity".format(unity_version) # Unity Hub (Linux)
    ]

    for path in possible_paths:
        fullpath = os.path.expanduser(path.replace("\"", ""))
        if os.path.exists(fullpath):
            unity_bin = fullpath
            break

if unity_bin:
    print("Found Unity: " + unity_bin)
else:
    print(
        "Failed to find Unity version "
        + unity_version
        + " - please install it via Unity Hub or u3d."
        + "Alternatively, set the UNITY_VERSION environment variable "
        + "to your preferred version and try again."
    )
    exit(1)

build_tag = os.environ.get("CI_COMMIT_TAG", "LocalDeploy-" + unity_version)

def export_path(package_name):
    from build_shared import safe_rel_path
    return safe_rel_path(
        package_name + "-" + build_tag + ".unitypackage"
    )

xcpretty_shell = "xcpretty" if is_tool("xcpretty") else "cat -"
