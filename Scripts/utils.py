import os

current_directory = [str(os.getcwd())]


def push_dir(d: str):
    if os.path.exists(os.path.join(curr_dir(), d)):
        current_directory.append(d)
    else:
        print("Path does not exist" + os.path.join(curr_dir(), d))
        exit(1)


def pop_dir():
    current_directory.pop()


def curr_dir():
    return os.path.join(*current_directory)
