#!/usr/bin/env bash

clean_unity_proj() {
	local dir="$1"

	# Delete anything not in Git
	pushd "${dir}"
		rm -rf *.unitypackage
		rm -rf *.gradle
		rm -rf Library/
		rm -rf Temp/
		rm -rf Obj/
		rm -rf Build/
		rm -rf Builds/
		rm -rf Logs/
		rm -rf UserSettings
		rm -rf MemoryCaptures
		rm -rf .gradle/
		rm -rf AndroidBuild/.gradle
		rm -rf AndroidBuild/build
		rm -rf iOSBuild/DerivedData
		rm -rf iOSBuild/Pods
	popd
}

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
. "${DIR}/constants.sh"
cd "${DIR}/.."

clean_unity_proj NESDKUnity
clean_unity_proj NESDKUnityDemo
