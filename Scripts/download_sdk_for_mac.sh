# Based Off: https://gist.github.com/warnyul/f12bcbb595542678b643e7aa9f0e68f6

#!/usr/bin/env bash
set -eou pipefail

. "constants.sh"

ANDROID_MAJOR=27
ANDROID_MINOR=0
ANDROID_PATH=3


brew uninstall --ignore-dependencies java
brew cask install homebrew/cask-versions/adoptopenjdk8
touch ~/.android/repositories.cfg
brew cask install android-sdk

#mkdir -p "$HOME/android-sdk-dl"
#if [[ ! -e "${HOME}/android-sdk-dl/sdk-tools.zip" ]]; then
#	  curl https://dl.google.com/android/repository/sdk-tools-darwin-4333796.zip > "$HOME/android-sdk-dl/sdk-tools.zip"
#fi

#if [[ ${IS_IN_TRAVIS} -eq 0 ]]; then
#	cond_yes="yes"
#else
#	cond_yes=""
#fi

#unzip -qq -n "$HOME/android-sdk-dl/sdk-tools.zip" -d "$ANDROID_HOME"
#  # Install or update Android SDK components (will not do anything if already up to date thanks to the cache mechanism)
#  "${cond_yes}" | "$ANDROID_HOME/tools/bin/sdkmanager" --licenses
#  "${cond_yes}" | "$ANDROID_HOME/tools/bin/sdkmanager" 'tools'
#  "${cond_yes}" | "$ANDROID_HOME/tools/bin/sdkmanager" 'platform-tools'
#  "${cond_yes}" | "$ANDROID_HOME/tools/bin/sdkmanager" "build-tools;${ANDROID_MAJOR}.${ANDROID_MINOR}.${ANDROID_PATCH#}"
#  "${cond_yes}" | "$ANDROID_HOME/tools/bin/sdkmanager" "platforms;android-${ANDROID_MAJOR}"
#  "${cond_yes}" | "$ANDROID_HOME/tools/bin/sdkmanager" "extras;google;m2repository"
