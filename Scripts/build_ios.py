#!/usr/bin/env python3
from constants import *
import os
import sys
import shutil
from distutils.dir_util import copy_tree
import subprocess
import argparse

from build_shared import *

def copy_plugins_proj(project_name):
    clear_plugins_shared(project_name)
    copy_plugins_shared(project_name)

    print("\n" + project_name + ", iOS: Copying wrapper plugin source")
    audiences_proj_plugins = safe_rel_path(project_name, "Assets", "NumberEight", "Audiences", "Plugins", "iOS")
    insights_proj_plugins = safe_rel_path(project_name, "Assets", "NumberEight", "Insights", "Plugins", "iOS")
    shared_proj_plugins = safe_rel_path(project_name, "Assets", "NumberEight", "Shared", "Plugins", "iOS")
    ios_audiences_plugin = safe_rel_path("Plugins", "iOS", "Audiences")
    ios_insights_plugin = safe_rel_path("Plugins", "iOS", "Insights")
    ios_shared_plugin = safe_rel_path("Plugins", "iOS", "Shared")

    os.makedirs(audiences_proj_plugins, exist_ok=True)
    os.makedirs(insights_proj_plugins, exist_ok=True)
    os.makedirs(shared_proj_plugins, exist_ok=True)

    copy_tree(ios_audiences_plugin, audiences_proj_plugins)
    copy_tree(ios_insights_plugin, insights_proj_plugins)
    copy_tree(ios_shared_plugin, shared_proj_plugins)

def copy_ios_sdk():
    copy_plugins_proj(sdk_name())

def copy_ios_demo():
    copy_plugins_proj(demo_name())


def create_fake_plist(project_name):
    ne_info_file = safe_rel_path(project_name, "Assets", "NumberEight-Info.plist")
    if not os.path.exists(ne_info_file):
        print(project_name + ": Creating Fake PList file")
        with open(ne_info_file, "w") as f:
            f.write(
                '<?xml version="1.0" encoding="UTF-8"?>\n \
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">\n \
<plist version="1.0">\n \
<dict>\n \
	<key>API_KEY</key>\n \
	<string>ABCDEFGHNIJKLMNOP</string>\n  \
</dict>\n \
</plist>'
            )


def build_ios_proj(project_name):
    print("\n" + project_name + ", iOS: Building iOS target")
    build_dir=safe_path(project_name, "iOSBuild")
    create_fake_plist(project_name)

    os.makedirs(safe_rel_path(build_dir), exist_ok=True)

    print("\n" + project_name + ", iOS: Building Unity platform")
    build_unity_platform(project_name, platform="iOS")

    print("\n" + project_name + ", iOS: Exporting Unity project")
    run_unity_method(project_name, method="NE.Utils.AutomatedBuild.PerformiOSBuild")

    # Unity is supposed to do this itself, however there are sometimes issues with it
    # that do not occur when running the command directly.
    print("\n" + project_name + ", iOS: Run Pod Install Manually")
    push_dir(safe_path(build_dir))
    safe_call(["pod install"])
    pop_dir()

    print("\n" + project_name + ", iOS: Building Unity Project")
    safe_call(
        [
            "set -eou pipefail && ",
            "xcodebuild",
            "CONFIGURATION=ReleaseForRunning",
            "-workspace",
            safe_path(build_dir, "Unity-iPhone.xcworkspace"),
            "-sdk",
            "iphoneos",
            "-scheme",
            '"Unity-iPhone"',
            "clean build",
            "|",
            xcpretty_shell,
        ]
    )
    pop_dir()


def build_ios_sdk():
    build_ios_proj(sdk_name())


def build_ios_demo():
    build_ios_proj(demo_name())


def build_ios(project_dir, build_sdk: bool, build_demo: bool):
    if sys.platform != 'darwin':
        print("iOS: Nothing to do, iOS can only be built on macOS")
        return 0

    if is_tool("xcodebuild"):
        print("iOS: Found XCode")
    else:
        print("iOS: Xcode not found, cannot proceed")
        return 1

    if is_tool("pod"):
        print("iOS: Found Cocoapods")
    else:
        print("iOS: Cocoapods not found, install with `brew install cocoapods`")
        return 1

    push_dir(project_dir)

    copy_ios_sdk()
    copy_ios_demo()

    if build_sdk:
        build_ios_sdk()

    if build_demo:
        build_ios_demo()

    pop_dir()

    return 0

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Prepare and build the iOS Unity projects.')
    parser.add_argument('project_dir',
                        help='directory in which the Unity projects are located')
    parser.add_argument('--build', dest='build', action='store_true',
                        help='prepares the plugins and builds NESDKUnity')
    parser.add_argument('--build-demo', dest='build_demo', action='store_true',
                        help='prepares the plugins and builds NESDKUnityDemo')
    parser.add_argument('--prepare', dest='build', action='store_false',
                        help='prepares the plugins without building the Unity projects')

    args = parser.parse_args()

    rc = build_ios(project_dir=args.project_dir, build_sdk=args.build, build_demo=args.build_demo)
    sys.exit(rc)
