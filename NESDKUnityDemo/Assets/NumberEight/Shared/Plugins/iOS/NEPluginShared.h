#import <Foundation/Foundation.h>

#include <string>
#include <sstream>

// From: https://stackoverflow.com/questions/37047781/how-to-return-string-from-native-ios-plugin-to-unity
static char* _Nullable convertNSStringToCString(const NSString* _Nullable nsString) {
    if (nsString == NULL) {
        return NULL;
    }
    
    const char* nsStringUtf8 = [nsString UTF8String];
    // create a null terminated C string on the heap so that our string's memory isn't wiped out right after method's return
    char* cString = (char*)malloc(strlen(nsStringUtf8) + 1);
    strcpy(cString, nsStringUtf8);
    
    return cString;
}

static NSString* _Nullable convertCStringToNSString(const char* _Nullable string) {
    if (string && strcmp(string, "") != 0) {
        return [NSString stringWithCString:string encoding:NSUTF8StringEncoding];
    }
    
    return nil;
}

static NSString* _Nullable convertCStringToNSStringOrDefault(const char* _Nullable string, const char* _Nonnull def) {
    if (auto converted = convertCStringToNSString(string)) {
        return converted;
    }
    
    return convertCStringToNSString(def);
}
