#import <Foundation/Foundation.h>

#import <Audiences/Audiences.h>
#import <NumberEightCompiled/NumberEightCompiled.h>

#include "NEPluginShared.h"
#include <string>
#include <sstream>

static NSString* const LOG_TAG = @"NEAudiencesPlugin";

namespace {
    static char* audiencesSetToStr(NSSet<NSString*>* set) {
        NSString* audiencesStr = [set.allObjects componentsJoinedByString:@","];
        return convertNSStringToCString(audiencesStr);
    }
}

extern "C" {
    void NEAudiences_startRecording(const char* apiKey) {
        NSString* nsApiKey = convertCStringToNSString(apiKey);
        NEXAPIToken* token = [NEXNumberEight startWithApiKey:nsApiKey launchOptions:nil consentOptions:[NEXConsentOptions useConsentManager]
          facingAuthorizationChallenges:^(NEXAuthorizationSource authSource, id<NEXAuthorizationChallengeResolver> _Nonnull resolver) {
            switch (authSource) {
            case kNEXAuthorizationSourceLocation:
                // Location permission requests are ignored - we get what we're given if we're given it
                //[resolver requestAuthorization];
                break;
            }
        } completion:^(BOOL success, NSError * _Nullable error) {
            if (success) {
                [NELog msg:LOG_TAG info:@"NumberEight Audiences: Successfully created token."];
            } else {
                [NELog msg:LOG_TAG warning:[NSString stringWithFormat:@"NumberEight Audiences failed to start: %@", error.debugDescription]];
            }
        }];

        [NELog msg:LOG_TAG info:@"Starting NumberEight Audiences"];
        [NEXAudiences startRecordingWithApiToken:token];
    }

    void NEAudiences_stopRecording() {
        [NELog msg:LOG_TAG info:@"Stopping NumberEight Audience Recording"];
        [NEXAudiences stopRecording];
    }

    void NEAudiences_pauseRecording() {
        [NELog msg:LOG_TAG info:@"Pausing NumberEight Audiences Recording"];
        [NEXAudiences pauseRecording];
    }

    char* NEAudiences_currentMemberships() {
        NSSet<NEXMembership*>* membershipsSet = [NEXAudiences currentMemberships];

        // Can't encode Audiences Membership object to JSON directly because of limitations with Apple's
        // JSONEncoder for Objective-C. Instead, we convert each object to a dictionary of plain types,
        // and serialize that.
        NSMutableArray* memberships = [NSMutableArray new];
        for (NEXMembership* member in membershipsSet.allObjects) {
            [memberships addObject:[member asDictionary]];
        }

        // Value type is an array of the encoded dictionaries, from NEXMembership.
        NSDictionary<NSString*, NSArray<NSDictionary*>*>* dict = @{@"memberships": memberships};

        NSError* error;
        NSData* jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:&error];

        if (!jsonData) {
            [NELog msg:LOG_TAG warning:[NSString stringWithFormat:@"NumberEight Audiences encountered an error while fetching audiences: %@", error]];
            return convertNSStringToCString(@"{\"memberships\": []}");
        }
        NSString* jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        return convertNSStringToCString(jsonString);
    }

    char* NEAudiences_currentIds() {
        return audiencesSetToStr([NEXAudiences currentIds]);
    }

    char* NEAudiences_currentExtendedIds() {
        return audiencesSetToStr([NEXAudiences currentExtendedIds]);
    }

    char* NEAudiences_currentIabIds() {
        return audiencesSetToStr([NEXAudiences currentIabIds]);
    }

    char* NEAudiences_currentExtendedIabIds() {
        return audiencesSetToStr([NEXAudiences currentExtendedIabIds]);
    }

    char* NEAudiences_deviceId() {
        NSString* deviceId = [NEXNumberEight deviceID];
        return convertNSStringToCString(deviceId);
    }

    void NEAudiences_deleteUserData() {
        [NELog msg:LOG_TAG info:@"Deleting user data"];
        [NEXNumberEight deleteUserData];
    }
}
