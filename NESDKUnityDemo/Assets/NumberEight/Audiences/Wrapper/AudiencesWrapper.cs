﻿using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

namespace NE.Audiences
{
    [HelpURL("http://docs.numbereight.ai")]
    public class AudiencesWrapper
    {
        private static System.Type AudiencesClass =
            Assembly.GetExecutingAssembly().GetType("NE.Audiences.Audiences");

        private static MethodInfo CurrentMembershipsMethod =
            AudiencesClass?.GetMethod("CurrentMemberships", BindingFlags.Public | BindingFlags.Static);

        private static MethodInfo CurrentIdsMethod =
            AudiencesClass?.GetMethod("CurrentIds", BindingFlags.Public | BindingFlags.Static);

		private static MethodInfo CurrentExtendedIdsMethod =
            AudiencesClass?.GetMethod("CurrentExtendedIds", BindingFlags.Public | BindingFlags.Static);

		private static MethodInfo CurrentIabIdsMethod =
            AudiencesClass?.GetMethod("CurrentIabIds", BindingFlags.Public | BindingFlags.Static);

		private static MethodInfo CurrentExtendedIabIdsMethod =
            AudiencesClass?.GetMethod("CurrentExtendedIabIds", BindingFlags.Public | BindingFlags.Static);

        /**
         * <returns>True if NumberEight Audiences is loaded in the current Unity application.</returns>
         */
        public static bool Exists()
        {
            return AudiencesClass != null;
        }

        /**
         * The list of audience memberships detected by NumberEight Audiences.
         * This list changes periodically as the user uses the device more.
         *
         * <returns>
         * A set of audience memberships for the user containing an ID, audience name, and
         * equivalent IAB Audience Taxonomy IDs.
         * e.g.
         *      - Membership("NE-1-1", "Joggers", [ IABAudience("410") ], "live")
         *      - Membership("NE-2-6", "Culture Vultures", [ IABAudience("779") ], "habitual")
         *      - Membership("NE-2-3", "Cinema Buffs", [
         *          IABAudience("467"),
         *          IABAudience("787", [ "PIFI3" ])
         *        ], "habitual")
         *
         * Note: an empty list will be returned if NumberEight Audiences isn't loaded in the current Unity application.
         * </returns>
         */
        public static List<Membership> CurrentMemberships()
        {
            return (List<Membership>) CurrentMembershipsMethod?.Invoke(null, null) ?? new List<Membership>();
        }

        /**
         * The list of audience IDs detected by NumberEight Audiences.
         * This is a convenience function to return only the IDs from the full list of memberships.
         *
         * <returns>
         * A set of audience membership IDs for the user.
         * e.g.
         * - "NE-1-1"
         * - "NE-2-6"
         * - "NE-2-3"
         *
         * Note: an empty list will be returned if NumberEight Audiences isn't loaded in the current Unity application.
         * </returns>
         */
        public static List<string> CurrentIds()
        {
            return (List<string>)CurrentIdsMethod?.Invoke(null, null) ?? new List<string>();
        }


        /**
         * The list of audience IDs and their corresponding liveness detected by NumberEight Audiences.
         * This is a convenience function to return only the extended IDs from the list of memberships.
         *
         * This is useful for use cases such as adding audience memberships to ad requests.
         *
         * <returns>
         * A set of extended audience membership IDs for the user.
         * e.g.
         * - "NE-1-1|H"
         * - "NE-100-1|L"
         * - "NE-101-2|T"
         *
         * Note: an empty list will be returned if NumberEight Audiences isn't loaded in the current Unity application.
         * </returns>
         */
        public static List<string> CurrentExtendedIds()
        {
            return (List<string>)CurrentExtendedIdsMethod?.Invoke(null, null) ?? new List<string>();
        }


        /**
         * The list of IAB Audience Taxonomy IDs detected by NumberEight Audiences.
         * This is a convenience function to return only the IAB IDs from the list of memberships.
         *
         * This is useful for use cases such as adding audience memberships to ad requests.
         *
         * <returns>
         * A set of IAB IDs for the user.
         * e.g.
         * - "408"
         * - "762"
         *
         * Note: an empty list will be returned if NumberEight Audiences isn't loaded in the current Unity application.
         * </returns>
         */
        public static List<string> CurrentIabIds()
        {
            return (List<string>)CurrentIabIdsMethod?.Invoke(null, null) ?? new List<string>();
        }


        /**
         * The list of extended IAB Audience Taxonomy IDs detected by NumberEight Audiences.
         * This is a convenience function to return the IAB IDs along with any extensions such
         * as purchase intent from the list of memberships.
         *
         * This format is as described by the IAB Seller-Defined Audiences guidance.
         *
         * This is useful for use cases such as adding audience memberships to ad requests.
         *
         * <returns>
         * A set of IAB IDs for the user.
         * e.g.
         * - "408|PIFI1"
         * - "762|PIFI2|PIPV2"
         *
         * Note: an empty list will be returned if NumberEight Audiences isn't loaded in the current Unity application.
         * </returns>
         */
        public static List<string> CurrentExtendedIabIds()
        {
            return (List<string>)CurrentExtendedIabIdsMethod?.Invoke(null, null) ?? new List<string>();
        }
    }
}
