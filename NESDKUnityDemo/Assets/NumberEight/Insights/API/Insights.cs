using System.Collections.Generic;
using System.Linq;
using UnityEngine;

#if UNITY_IOS
using System.Runtime.InteropServices; // Includes DllImport
#endif

namespace NE.Insights
{
    [HelpURL("http://docs.numbereight.ai")]
    public class Insights : MonoBehaviour
    {
        static Insights instance;
        private static Insights Instance
        {
            get
            {
                if (instance == null)
                {
                    GameObject go = new GameObject("Insights");
                    instance = go.AddComponent<Insights>();
                }
                return instance;
            }
        }

        [SerializeField]
        public bool isEnabled = true;

        [SerializeField]
        public bool startAutomatically = true;

        [SerializeField]
        public string apiKey;

#if UNITY_IOS
        [DllImport("__Internal")]
        private static extern string NEInsights_deviceId();

        [DllImport("__Internal")]
        private static extern void NEInsights_startRecording(string apiKey, string recordingConfigJson);

        [DllImport("__Internal")]
        private static extern void NEInsights_stopRecording();

        [DllImport("__Internal")]
        private static extern void NEInsights_pauseRecording();

        [DllImport("__Internal")]
        private static extern bool NEInsights_addMarker(string name);

        [DllImport("__Internal")]
        private static extern void NEInsights_deleteUserData();

        [DllImport("__Internal")]
        private static extern string NEInsights_defaultRecordingConfig();

#elif UNITY_ANDROID
        const string pluginName = "ai.numbereight.insightsunity.InsightsPlugin";
        static AndroidJavaClass _pluginClass;
        static AndroidJavaObject _pluginInstance;

        private static AndroidJavaClass PluginClass
        {
            get
            {
                if (_pluginClass == null)
                {
                    _pluginClass = new AndroidJavaClass(pluginName);
                }
                return _pluginClass;
            }
        }

        private static AndroidJavaObject PluginInstance
        {
            get
            {
                if (_pluginInstance == null)
                {
                    _pluginInstance = new AndroidJavaObject(pluginName);
                }
                return _pluginInstance;
            }
        }
#endif

        void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }
            else if (instance != this)
            {
                Destroy(gameObject);
            }
            DontDestroyOnLoad(gameObject);
        }

        void Start()
        {
            if (Instance.startAutomatically)
            {
                StartRecording();
            }
        }


        public static RecordingConfig defaultRecordingConfig()
        {
#if UNITY_IOS && !UNITY_EDITOR
            string jsonString = NEInsights_defaultRecordingConfig();
            return JsonUtility.FromJson<RecordingConfig>(jsonString);
#elif UNITY_ANDROID && !UNITY_EDITOR
            string jsonString = PluginInstance.Call<string>("defaultRecordingConfig");
            return JsonUtility.FromJson<RecordingConfig>(jsonString);
#else
            return new RecordingConfig();
#endif
        }

        /**
         * Starts recording device usage to categorise the user into audiences.
         *
         * This will associate an ID with the current device. If NumberEight Insights is in use
         * with a suitable device_id, then that will be used. Otherwise, a new UUID will be created
         * and stored locally.
         *
         * If the SDK is disabled, this will do nothing.
         *
         * Note: due to the mandatory use of a device ID, this data will be treated as
         * Personally Identifiable Information until future releases remove the need for an ID.
         */
        public static void StartRecording()
        {
            if (Instance.isEnabled)
            {
                string jsonString = JsonUtility.ToJson(RecordingConfig.defaultConfig());

#if UNITY_IOS && !UNITY_EDITOR
                NEInsights_startRecording(Instance.apiKey, jsonString);
#elif UNITY_ANDROID && !UNITY_EDITOR
                AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
                AndroidJavaObject activity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
                AndroidJavaObject context = activity.Call<AndroidJavaObject>("getApplicationContext");
                PluginInstance.Call("startRecording", context, Instance.apiKey, jsonString);
#endif
            }
        }

        /**
         * Starts recording device usage to categorise the user into audiences.
         *
         * This will associate an ID with the current device. If NumberEight Insights is in use
         * with a suitable device_id, then that will be used. Otherwise, a new UUID will be created
         * and stored locally.
         *
         * If the SDK is disabled, this will do nothing.
         *
         * Note: due to the mandatory use of a device ID, this data will be treated as
         * Personally Identifiable Information until future releases remove the need for an ID.
         */
        public static void StartRecording(RecordingConfig config)
        {
            if (Instance.isEnabled)
            {
                string jsonString = JsonUtility.ToJson(config);

#if UNITY_IOS && !UNITY_EDITOR
                NEInsights_startRecording(Instance.apiKey, jsonString);
#elif UNITY_ANDROID && !UNITY_EDITOR
                AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
                AndroidJavaObject activity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
                AndroidJavaObject context = activity.Call<AndroidJavaObject>("getApplicationContext");
                PluginInstance.Call("startRecording", context, Instance.apiKey, jsonString);
#endif
            }
        }

        /**
         * Stops recording device usage for audiences.
         * Will still stop if `isEnabled` is false.
         */
        public static void StopRecording()
        {
            // You should be able to stop the SDK even if it's `disabled`.
            // Could be disabled while running, should still stop.
            // Will not automatically stop when disabled while running.
#if UNITY_IOS && !UNITY_EDITOR
            NEInsights_stopRecording();
#elif UNITY_ANDROID && !UNITY_EDITOR
            PluginInstance.Call("stopRecording");
#endif
        }

        /**
         * Create a marker at the current time to tag a particular app event with the current user context.
         *
         * Some ideas:
         * ```
         * Insights.AddMarker("screen_viewed"); // Put inside onResume()
         * Insights.AddMarker("in_app_purchase"); // Put inside an in-app purchase handler
         * Insights.AddMarker("share"); // Put inside a handler for sharing on social media
         * ```
         *
         * This function will trigger an upload if the user is currently connected to Wi-Fi.
         *
         * @param name An identifier for the marker.
         *
         * @return false if no recording session is currently active.
         */
        public static bool AddMarker(string name)
        {
            if (!Instance.isEnabled)
            {
                return false;
            }

#if UNITY_IOS && !UNITY_EDITOR
            return NEInsights_addMarker(name);
#elif UNITY_ANDROID && !UNITY_EDITOR
            return PluginInstance.Call<bool>("addMarker", name);
#else
            return false;
#endif
        }


        /**
         * A device identifier for use in security and error reporting
         */
        public static string DeviceId()
        {
#if UNITY_IOS && !UNITY_EDITOR
            return NEInsights_deviceId();
#elif UNITY_ANDROID && !UNITY_EDITOR
            AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject activity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
            AndroidJavaObject context = activity.Call<AndroidJavaObject>("getApplicationContext");
            return PluginInstance.Call<string>("getDeviceId", context);
#elif UNITY_EDITOR
            return "The NumberEight Device ID is not available in the Unity Editor.";
#endif
        }

        /**
         * Delete NumberEight's data from the device's local storage.
         *
         * Warning: this will remove all data related to NumberEight, including device identifiers.
         * It will no longer be possible to cross-reference this user to logs or records of consent.
         * Consequently, this will also anonymise any data that NumberEight has stored on its servers.
         */
        public static void DeleteUserData()
        {
#if UNITY_IOS && !UNITY_EDITOR
            NEInsights_deleteUserData();
#elif UNITY_ANDROID && !UNITY_EDITOR
            AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject activity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
            AndroidJavaObject context = activity.Call<AndroidJavaObject>("getApplicationContext");
            PluginInstance.Call("deleteUserData", context);
#endif
        }
    }
}
