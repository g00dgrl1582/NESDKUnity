using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using System;

namespace NE.Insights
{
    [HelpURL("http://docs.numbereight.ai")]
    public class InsightsWrapper
    {
        private static System.Type InsightsClass =
            Assembly.GetExecutingAssembly().GetType("NE.Insights.Insights");

        private static MethodInfo AddMarkerMethod =
            InsightsClass?.GetMethod("AddMarker", BindingFlags.Public | BindingFlags.Static);

        /**
         * <returns>True if NumberEight Insights is loaded in the current Unity application.</returns>
         */
        public static bool Exists()
        {
            return InsightsClass != null;
        }

        /**
         * Create a marker at the current time to tag a particular app event with the current user context.
         *
         * Some ideas:
         * ```
         * Insights.AddMarker("screen_viewed"); // Put inside onResume()
         * Insights.AddMarker("in_app_purchase"); // Put inside an in-app purchase handler
         * Insights.AddMarker("share"); // Put inside a handler for sharing on social media
         * ```
         *
         * This function will trigger an upload if the user is currently connected to Wi-Fi.
         *
         * @param name An identifier for the marker.
         *
         * <returns> false if no recording session is currently active. </returns>
		 *
		 * Note: This is a noop and returns false if NumberEight Insights isn't loaded in the current Unity application.
         */
        public static bool AddMarker(string name)
        {
            if (Exists())
            {
                return false;
            }
            object result = AddMarkerMethod?.Invoke(null, new object[]{name});
            return Convert.ToBoolean(result);
        }
    }
}
