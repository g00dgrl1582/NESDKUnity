package ai.numbereight.insightsunity;

import android.content.Context;
import android.os.Bundle;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import ai.numbereight.insights.Insights;
import ai.numbereight.insights.RecordingConfig;
import ai.numbereight.sdk.ConsentOptions;
import ai.numbereight.sdk.NumberEight;
import ai.numbereight.sdk.common.Log;

@SuppressWarnings("unused")
class InsightsPlugin {
    private static final String LOG_TAG = "InsightsPlugin";

    // From: https://stackoverflow.com/questions/28984789/convert-json-to-android-bundle/35734150
    private static Bundle jsonStringToBundle(String jsonString) {
        try {
            JSONObject jsonObject = new JSONObject(jsonString);
            return jsonToBundle(jsonObject);
        } catch (JSONException ex) {
            Log.w(LOG_TAG, "Failed to convert json string to bundle", ex);
        }

        return null;
    }

    // From: https://stackoverflow.com/questions/28984789/convert-json-to-android-bundle/35734150
    private static Bundle jsonToBundle(JSONObject jsonObject) throws JSONException {
        Bundle bundle = new Bundle();
        Iterator<String> iter = jsonObject.keys();

        while (iter.hasNext()) {
            // This assumes every value is a string.
            // This is fine because we only accept Dictionary<String, String> in C#
            // But this will need to be revisited if we ever were to change that.
            String key = iter.next();
            String value = jsonObject.getString(key);
            Bundle bundleVal = jsonStringToBundle(value);

            if (bundleVal != null) {
                bundle.putBundle(key, bundleVal);
            } else {
                bundle.putString(key, value);
            }
        }

        return bundle;
    }

    private static String recordingConfigToJSONString(RecordingConfig config) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("deviceId", config.getDeviceId());
        jsonObject.put("uploadWithWifiOnly", config.getUploadWithWifiOnly());
        jsonObject.put("initialUploadDelay", config.getInitialUploadDelay() / 1000);
        jsonObject.put("uploadInterval",  config.getUploadInterval() / 1000);
        jsonObject.put("mostProbableOnly", config.getMostProbableOnly());

        jsonObject.put("topics", new JSONArray(config.getTopics()));

        // Need to serialize the filters this way (key[], value[]) because Unity does not support serializing dictionaries.
        JSONArray filterKeysArr = new JSONArray();
        JSONArray filterValsArr = new JSONArray();
        for (Map.Entry<String, String> entry : config.getFilters().entrySet()) {
            filterKeysArr.put(entry.getKey());
            filterValsArr.put(entry.getValue());
        }

        JSONObject filtersObj = new JSONObject();
        filtersObj.put("keys", filterKeysArr);
        filtersObj.put("values", filterValsArr);
        jsonObject.put("filters", filtersObj);

        return jsonObject.toString();
    }

    private static RecordingConfig jsonStringToRecordingConfig(String jsonString) throws JSONException {
        JSONObject jsonObject = new JSONObject(jsonString);
        String deviceId = jsonObject.getString("deviceId");
        boolean uploadWithWifiOnly = jsonObject.getBoolean("uploadWithWifiOnly");
        long initialUploadDelay = Double.valueOf(jsonObject.getDouble("initialUploadDelay") * 1000).longValue();
        long uploadInterval = Double.valueOf(jsonObject.getDouble("uploadInterval") * 1000).longValue();
        boolean mostProbableOnly = jsonObject.getBoolean("mostProbableOnly");

        JSONArray topicsArr = jsonObject.getJSONArray("topics");
        List<String> topics = new ArrayList<>();
        for (int i = 0; i < topicsArr.length(); i++) {
            topics.add(topicsArr.getString(i));
        }

        // Filters <String, String>
        JSONObject filtersObj = jsonObject.getJSONObject("filters");
        Map<String, String> filters = new HashMap<String, String>();
        JSONArray keys = filtersObj.getJSONArray("keys");
        JSONArray values = filtersObj.getJSONArray("values");
        for (int i = 0; i < Math.min(keys.length(), values.length()); i++) {
            String key = keys.getString(i);
            String value = values.getString(i);
            filters.put(key, value);
        }

        return new RecordingConfig(deviceId, uploadWithWifiOnly, initialUploadDelay,
                                   uploadInterval, mostProbableOnly, topics, filters);
    }

    void startRecording(Context context, String apiKey, String recordingConfigJson) {
        Log.i(LOG_TAG, "Starting NumberEight Insights Recording");
        NumberEight.APIToken token = NumberEight.start(apiKey, context, ConsentOptions.useConsentManager());

        try {
            RecordingConfig config = jsonStringToRecordingConfig(recordingConfigJson);
            Insights.startRecording(token, config);
        } catch (JSONException error) {
            Log.e(LOG_TAG, "Failed to parse recording config. Did not start Insights.", error);
        }
    }

    void stopRecording() {
        Log.i(LOG_TAG, "Stopping NumberEight Insights Recording");
        Insights.stopRecording();
    }

    void pauseRecording() {
        Log.i(LOG_TAG, "Pausing NumberEight Insights Recording");
        Insights.stopRecording();
    }

    boolean addMarker(String name) {
        return Insights.addMarker(name);
    }

    String defaultRecordingConfig() {
        try {
            return recordingConfigToJSONString(new RecordingConfig());
        } catch (JSONException error) {
            return "";
        }
    }

    String getDeviceId(Context context) {
        return NumberEight.getDeviceId(context);
    }

    void deleteUserData(Context context) {
        Log.i(LOG_TAG, "Deleting user data");
        NumberEight.deleteUserData(context);
    }
}
