package ai.numbereight.audiencesunity;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.Collection;
import java.util.Set;

import ai.numbereight.audiences.Audiences;
import ai.numbereight.audiences.Membership;
import ai.numbereight.sdk.ConsentOptions;
import ai.numbereight.sdk.NumberEight;
import ai.numbereight.sdk.common.Log;
import ai.numbereight.sdk.platform.FileService;

@SuppressWarnings("unused")
class AudiencesPlugin {
    private static final String LOG_TAG = "AudiencesPlugin";

    private static String join(CharSequence delim, Collection<String> elements) {
        if (elements.size() == 0) {
            return "";
        }

        StringBuilder sb = new StringBuilder();
        for (CharSequence c : elements) {
            sb.append(c);
            sb.append(delim);
        }
        int length = sb.length();
        sb.delete(length - delim.length(), length);
        return sb.toString();
    }

    void startRecording(Context context, String apiKey) {
        Log.i(LOG_TAG, "Starting NumberEight Audiences Recording");
        NumberEight.APIToken token = NumberEight.start(apiKey, context, ConsentOptions.useConsentManager());
        Audiences.startRecording(token);
    }

    void stopRecording() {
        Log.i(LOG_TAG, "Stopping NumberEight Audiences Recording");
        Audiences.stopRecording();
    }

    void pauseRecording() {
        Log.i(LOG_TAG, "Pausing NumberEight Audiences Recording");
        Audiences.pauseRecording();
    }

    String getCurrentMemberships() {
        Set<Membership> memberships = Audiences.getCurrentMemberships();
        JSONObject output = new JSONObject();

        try {
            JSONArray array = new JSONArray();
            output.put("memberships", array);

            for (Membership membership : memberships) {
                array.put(new JSONObject(membership.serialize()));
            }
        } catch (Exception ex) {
            Log.e(LOG_TAG, "Failed to encode memberships as JSON", ex);
        }

        return output.toString().replaceAll("iab_ids", "iabIds");
    }

    String getCurrentIds() {
        Set<String> ids = Audiences.getCurrentIds();
        return join(",", ids);
    }

    String getCurrentExtendedIds() {
        Set<String> ids = Audiences.getCurrentExtendedIds();
        return join(",", ids);
    }

    String getCurrentIabIds() {
        Set<String> ids = Audiences.getCurrentIabIds();
        return join(",", ids);
    }

    String getCurrentExtendedIabIds() {
        Set<String> ids = Audiences.getCurrentExtendedIabIds();
        return join(",", ids);
    }

    String getDeviceId(Context context) {
        return NumberEight.getDeviceId(context);
    }

    void delete(File root) {
        File[] list = root.listFiles();

        if (list == null) return;

        for (File f : list) {
            if (f.isDirectory()) {
                delete(f);
            } else {
                String name = f.getName();
                long bytes = f.length();
                long kilobytes = (bytes / 1024);
                boolean deleted = f.delete();
                Log.e(LOG_TAG, "Deleting file " + f.getName() + " - " + kilobytes + "KB: " + (deleted ? "SUCCESS" : "FAILED"));
            }
        }
    }

    void deleteUserData(Context context) {
        Log.i(LOG_TAG, "Deleting user data");
        File f = new File(context.getFilesDir(), "numbereight").getAbsoluteFile();
        delete(f);
    }
}
