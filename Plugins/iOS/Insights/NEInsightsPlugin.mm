#import <Foundation/Foundation.h>

#import <Insights/Insights.h>
#import <NumberEightCompiled/NumberEightCompiled.h>

#include "NEPluginShared.h"
#include <string>
#include <sstream>

static NSString* const LOG_TAG = @"NEInsightsPlugin";

namespace {
    static NEXRecordingConfig* _Nullable recordingConfigFromDictionary(NSDictionary* dict) {
        // C# separates out keys & values, merge them back together before giving to iOS
        NSDictionary<NSString*, id>* filtersParsed = dict[@"filters"];
        NSArray<NSString*>* filtersKeys = filtersParsed[@"keys"];
        NSArray<NSString*>* filtersValues = filtersParsed[@"values"];

        NSMutableDictionary* dictMut = [NSMutableDictionary dictionaryWithDictionary:dict];
        dictMut[@"topics"] = [[NSSet alloc] initWithArray: dict[@"topics"]];
        dictMut[@"filters"] = [NSDictionary dictionaryWithObjects:filtersValues forKeys:filtersKeys];

        return [NEXRecordingConfig fromDictionary:dictMut];
    }

    static NEXRecordingConfig* _Nullable recordingConfigFromJson(NSString* string) {
        NSData* strData = [string dataUsingEncoding:NSUTF8StringEncoding];
        if (!strData) {
            [NELog msg:LOG_TAG error:@"Got nil data while converting JSON to RecordingConfig"];
            return nil;
        }

        NSError* error = nil;
        NSDictionary* dict = [NSJSONSerialization JSONObjectWithData:strData options:0 error:&error];
        if (error) {
            [NELog msg:LOG_TAG error:[NSString stringWithFormat:@"Failed to parse string to JSON Object for RecordingConfig:fromJSONString (%@) Error:(%@)", string, error.debugDescription]];
            return nil;
        }

        return recordingConfigFromDictionary(dict);
    }
}

extern "C" {
    void NEInsights_startRecording(const char* _Nonnull apiKey, const char* _Nullable recordingConfigJson) {
        NSString* nsApiKey = convertCStringToNSString(apiKey);
        NEXAPIToken* token = [NEXNumberEight startWithApiKey:nsApiKey launchOptions:nil consentOptions:[NEXConsentOptions useConsentManager]
          facingAuthorizationChallenges:^(NEXAuthorizationSource authSource, id<NEXAuthorizationChallengeResolver> _Nonnull resolver) {
            switch (authSource) {
            case kNEXAuthorizationSourceLocation:
                // Location permission requests are ignored - we get what we're given if we're given it
                //[resolver requestAuthorization];
                break;
            }
        } completion:^(BOOL success, NSError * _Nullable error) {
            if (success) {
                [NELog msg:LOG_TAG info:@"NumberEight Insights: Successfully created token"];
            } else {
                [NELog msg:LOG_TAG warning:[NSString stringWithFormat:@"NumberEight Insights failed to start: %@", error.debugDescription]];
            }
        }];

        [NELog msg:LOG_TAG info:@"Starting NumberEight Insights"];
        NSString* nsRecordingConfigJSON = convertCStringToNSStringOrDefault(recordingConfigJson, "{}");
        NEXRecordingConfig* config = recordingConfigFromJson(nsRecordingConfigJSON);
        [NEXInsights startRecordingWithAPIToken:token config:config];
    }

    char* NEInsights_defaultRecordingConfig() {
        NEXRecordingConfig* defaultConfig = [NEXRecordingConfig defaultConfig];

        NSMutableDictionary* dict = [NSMutableDictionary dictionaryWithDictionary:[defaultConfig asDictionary]];
        {
            NSArray<NSString*>* filterKeys = [(NSDictionary*)dict[@"filters"] allKeys];
            NSArray<NSString*>* filterValues = [(NSDictionary*)dict[@"filters"] allValues];

            // Patch in dict to separate keys & values for Unity
            dict[@"filters"] = @{@"keys": filterKeys, @"values": filterValues};
        }
        
        NSError* error;
        NSData* jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:&error];

        if (!jsonData) {
            [NELog msg:LOG_TAG error:[NSString stringWithFormat:@"NumberEight Insights encountered an error while serializing the default RecordingConfig: %@", error]];
            return convertNSStringToCString(@"{}");
        }
        NSString* jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        return convertNSStringToCString(jsonString);
    }

    void NEInsights_stopRecording() {
        [NELog msg:LOG_TAG info:@"Stopping NumberEight Insights Recording"];
        [NEXInsights stopRecording];
    }

    void NEInsights_pauseRecording() {
        [NELog msg:LOG_TAG info:@"Pausing NumberEight Insights Recording"];
        [NEXInsights pauseRecording];
    }

    bool NEInsights_addMarker(const char* _Nonnull name) {
        NSString* nameNS = convertCStringToNSString(name);

        NSError* error = nil;
        auto result = [NEXInsights addMarkerWithName:nameNS error:&error];
        if (error) {
            [NELog msg:LOG_TAG warning:[NSString stringWithFormat:@"NumberEight Insights encountered an error while adding a marker: %@", error.debugDescription]];
        }

        return result;
    }

    char* NEInsights_deviceId() {
        NSString* deviceId = [NEXNumberEight deviceID];
        return convertNSStringToCString(deviceId);
    }

    void NEInsights_deleteUserData() {
        [NELog msg:LOG_TAG info:@"Deleting user data"];
        [NEXNumberEight deleteUserData];
    }
}
