using UnityEngine;
using UnityEditor;

namespace NE.Audiences
{
    [HelpURL("http://docs.numbereight.ai")]
    [CustomEditor(typeof(Audiences))]
    internal class AudiencesEditor : Editor
    {
        #region Types -> Private

        private enum SerializedLocation
        {
            Default, Custom
        }

        #endregion

        #region Variables -> Private

        private Audiences instance;

        private Texture2D neLogo;

        #endregion

        #region Methods -> Unity Callbacks

        private void OnEnable()
        {
            instance = (Audiences)target;
            
            string skin = EditorGUIUtility.isProSkin ? "white.png" : "colour.png";
            neLogo = AssetDatabase.LoadAssetAtPath<Texture2D>("Assets/NumberEight/Audiences/Editor/Logo_" + skin);
        }

        #endregion

        #region Methods -> Helper Functions

        private void UpdateSetting<T>(ref T setting, T newValue)
        {
            if (!newValue.Equals(setting))
            {
                setting = newValue;
                EditorUtility.SetDirty(instance);
            }
        }

        #endregion

        #region Methods -> Public Override

        public override void OnInspectorGUI()
        {
            if (instance == null) {
                return;
            }

            if (neLogo != null)
            {
                GUILayout.Label(
                    neLogo,
                    new GUIStyle(GUI.skin.GetStyle("Label"))
                    {
                        alignment = TextAnchor.UpperCenter
                    }
                );
            }
            else
            {
                GUILayout.Label(
                    "NumberEight Audiences Configuration",
                    style: new GUIStyle(GUI.skin.GetStyle("Label"))
                    {
                        alignment = TextAnchor.UpperCenter,
                        fontSize = 16,
                        fontStyle = FontStyle.Bold
                    }
                );
            }

            GUILayout.Space(20);

            /// Is Enabled toggle
            bool isEnabled = EditorGUILayout.Toggle(
                new GUIContent(
                    text: "Enable NumberEight Audiences",
                    tooltip: "Enable or disable NumberEight Audiences in this build"
                ),
                value: instance.isEnabled
            );
            UpdateSetting(ref instance.isEnabled, isEnabled);

            EditorGUI.BeginDisabledGroup(!isEnabled);

            /// Start Automatically toggle
            bool startAutomatically = EditorGUILayout.Toggle(
                new GUIContent(
                    text: "Start automatically",
                    tooltip: "Let NumberEight start without calling Audiences.StartRecording()"
                ),
                value: instance.startAutomatically
            );
            UpdateSetting(ref instance.startAutomatically, startAutomatically);

            GUILayout.Space(20);

            /// API Key field
            string newKey = EditorGUILayout.TextField(
                new GUIContent(
                    text: "API key",
                    tooltip: "(required) A NumberEight API key registered to your account"
                ),
                text: instance.apiKey
            );
            UpdateSetting(ref instance.apiKey, newKey);

            EditorGUI.EndDisabledGroup();

            GUILayout.Space(20);

            GUILayout.BeginHorizontal();

            GUILayout.FlexibleSpace();

            if (GUILayout.Button("Get API Key", style:
                new GUIStyle(GUI.skin.GetStyle("Button"))
                {
                    fixedWidth = 150
                }))
            {
                Application.OpenURL("https://portal.eu.numbereight.ai/keys");
            }

            if (GUILayout.Button("Documentation", style:
                new GUIStyle(GUI.skin.GetStyle("Button"))
                {
                    fixedWidth = 150
                }))
            {
                Application.OpenURL("https://portal.eu.numbereight.ai/documentation/#/unity-audiences");
            }

            GUILayout.FlexibleSpace();

            GUILayout.EndHorizontal();

            GUILayout.Space(20);

            GUILayout.Label(
                "© 2021 NumberEight Technologies Ltd\n" +
                "NumberEight participates in the IAB Europe Transparency & Consent Framework\n" +
                "and complies with its Specifications and Policies.\n" +
                "NumberEight’s identification number within the framework is 882.",
                style: new GUIStyle(GUI.skin.GetStyle("Label"))
                {
                    alignment = TextAnchor.UpperCenter,
                    fontSize = 9,
                    fontStyle = FontStyle.Normal
                }
            );
        }

        #endregion
    }
}
