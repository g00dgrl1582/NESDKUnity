using System;
using System.Collections.Generic;
using UnityEditor;

namespace NE.Utils
{
    public class AutomatedBuild
    {
        static string[] SCENES = FindEnabledEditorScenes();

        static string TARGET_DIR = ".";

        static void PerformiOSBuild()
        {
            try
            {
                BuildOptions iosBuildOptions = BuildOptions.CompressWithLz4;
                GenericBuild(SCENES, TARGET_DIR + "/iOSBuild", BuildTargetGroup.iOS, BuildTarget.iOS, iosBuildOptions);
            }
            catch (Exception e)
            {
                Console.Error.WriteLine(e.Message);
            }
        }

        static void PerformAndroidBuild()
        {
            try
            {
                BuildOptions androidBuildOptions = BuildOptions.CompressWithLz4;
                GenericBuild(SCENES, TARGET_DIR + "/AndroidBuild", BuildTargetGroup.Android, BuildTarget.Android, androidBuildOptions);
            }
            catch (Exception e)
            {
                Console.Error.WriteLine(e.Message);
            }
        }

        private static string[] FindEnabledEditorScenes()
        {
            List<string> EditorScenes = new List<string>();
            foreach (EditorBuildSettingsScene scene in EditorBuildSettings.scenes)
            {
                if (!scene.enabled)
                {
                    continue;
                }
                EditorScenes.Add(scene.path);
            }
            return EditorScenes.ToArray();
        }

        static void GenericBuild(string[] scenes, string targetDir, BuildTargetGroup buildTargetGroup, BuildTarget buildTarget, BuildOptions buildOptions)
        {
            EditorUserBuildSettings.SwitchActiveBuildTarget(buildTargetGroup, buildTarget);
            BuildPipeline.BuildPlayer(scenes, targetDir, buildTarget, buildOptions);
        }
    }
}
