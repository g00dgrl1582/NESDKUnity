using System;
using System.Collections.Generic;
using UnityEditor;

namespace NE.Utils
{
    public class Exporter
    {
        static void ExportPackage() {
			string root = "Assets/NumberEight";
            string[] items = {
				// Audiences
				root+"/Audiences/Audiences.prefab",
				root+"/Audiences/API", root+"/Audiences/Common", root+"/Audiences/Editor", root+"/Audiences/Plugins",
				root+"/Audiences/LICENSE.md", root+"/Audiences/README.md",
				// Insights
				root+"/Insights/Insights.prefab",
				root+"/Insights/API", root+"/Insights/Common", root+"/Insights/Editor", root+"/Insights/Plugins",
				root+"/Insights/LICENSE.md", root+"/Insights/README.md",
				// Common
				root+"/Shared",
				"Assets/ExternalDependencyManager"
			};
            AssetDatabase.ExportPackage(items, "NESDKUnity.unitypackage", ExportPackageOptions.Recurse);
        }

		static void ExportWrapperPackage() {
			string root = "Assets/NumberEight";
            string[] items = {
				// Audiences
				root+"/Audiences/Common", root+"/Audiences/Wrapper",
				root+"/Audiences/LICENSE.md", root+"/Audiences/README.md",
				// Insights
				root+"/Insights/Common", root+"/Insights/Wrapper",
				root+"/Insights/LICENSE.md", root+"/Insights/README.md"
			};
            AssetDatabase.ExportPackage(items, "NESDKUnityWrapper.unitypackage", ExportPackageOptions.Recurse);
        }
    }
}
