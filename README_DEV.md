# NESDKUnity Developer README

# Requirements
* Unity 2020.1.15f1
* macOS (if building for iOS)

## Installing Unity
Install [u3d](https://github.com/DragonBox/u3d) (recommended)
To install the correct version of Unity use: `u3d install <UNITY_VERSION> -p Unity,iOS,Android`

## Developing
Only update scripts in `Plugins/`.

* `Plugins/Android` contains the Android wrapper project.
* `Plugins/iOS` contains the iOS wrapper.
* `Plugins/Shared` contains the Unity project.

When ready, run the `bootstrap.sh` script which will copy the resources to the right location.

## Building SDK (Android & iOS)
If you are on a Mac, it will build iOS & Android, otherwise it will only build Android.
`./build.sh`

By default, the build script will build the SDK for Android & iOS, as well as the demo on Android/iOS (where relevant).

You need to put the `NumberEight-Info.plist` file in `Assets/NumberEight-Info.plist` for the `NESDKUnity/` and `NESDKUnityDemo/` projects if not manually entering an API key.

## Updating SDK Version
Both iOS and Android SDK dependency versions can be updated in the dependencies file `Plugins/Shared/ExternalDependencyManager/Editor/AudiencesDependencies.xml`.
You will need to open Unity to reload the dependencies for now until that step is done automatically by the build script (09/12/20).
